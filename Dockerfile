FROM node:carbon

# File Author / Maintainer
LABEL authors="Shaun McNamee <shaunmcnamee@gmail.com>"

# Set config environment
ARG config_env
ENV CONFIG_ENV=${config_env}

# Install app dependencies
COPY package.json /www/package.json
COPY package-lock.json /www/package-lock.json
RUN cd /www; npm install

# Copy app source
COPY . /www
COPY ./config/ ./www/config/
#RUN cd /www; node get-config.js ${config_env}

# Set work directory to /www
WORKDIR /www

# set your port
ENV PORT 8080

# expose the port to outside world
EXPOSE  8080

# start command as per package.json
CMD ["npm", "start"]
