const {spawn} = require('child_process');
const path = require('path');
const fs = require('fs');
const AWS = require('aws-sdk');

const S3_BUCKET = 'lsps-config';
const S3_KEY = `${process.argv[2]}.yaml`;

const DIR_NAME = 'config';
const DEST_DIR = path.join(process.cwd(), DIR_NAME);
const DEST_FILE = path.join(DEST_DIR, `${process.argv[2]}.yaml`);

const AWS_REGION = 'us-east-1';
const {AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY} = process.env;

if (!AWS_ACCESS_KEY_ID || !AWS_SECRET_ACCESS_KEY) {
  throw new Error(
    'Ensure environment variables are set: AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY'
  );
}

const mkdir = () =>
  new Promise((resolve, reject) => {
    const pDir = spawn('mkdir', ['-p', DIR_NAME]);

    pDir.on('exit', resolve);
    pDir.on('error', reject);
  });

const getFile = () =>
  new Promise((resolve, reject) => {
    console.log(`Downloading config file - key: ${S3_KEY}.`);
    const s3 = new AWS.S3();
    const options = {
      Bucket: S3_BUCKET,
      Key: S3_KEY,
    };
    s3.getObject(options, (err, response) => {
      err ? reject(err) : resolve(response.Body.toString());
    });
  });

const writeFile = contents =>
  new Promise((resolve, reject) => {
    console.log('Writing config file.');
    console.log(DEST_FILE);
    fs.writeFile(DEST_FILE, contents, err => {
      err ? reject(err) : resolve(DEST_FILE);
    });
  });

// Configure the SDK for this repo.
AWS.config.update({
  accessKeyId: AWS_ACCESS_KEY_ID,
  secretAccessKey: AWS_SECRET_ACCESS_KEY,
  region: AWS_REGION,
});

mkdir()
  .then(getFile)
  .then(writeFile)
  .catch(err => {
    console.error(err);

    //Allow process to gracefully exit with exit code instead of
    //synchronously exiting.
    //If we need immediate exit due to an error state, we can throw
    //an uncaught error.
    //See https://nodejs.org/api/process.html#process_process_exit_code
    process.exitCode = 1;
  });

