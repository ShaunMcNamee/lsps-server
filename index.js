const http = require('http');
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const initializeDb = require('./src/db');
const config = require('./config');
const prepModels = require('./src/models/index');
const routes = require('./src/routes/index');

let app = express();
app.server = http.createServer(app);

// logger
app.use(morgan('dev'));

// 3rd party middleware
app.use(
  cors({
    exposedHeaders: config.http.corsHeaders,
  })
);

app.use(
  bodyParser.json({
    limit: config.http.bodyLimit,
  })
);

// connect to db
initializeDb(db => {
  const models = prepModels(db);

  app.use('/', routes(models));

  app.server.listen(8080, () => {
    console.log(`Started on port 8080`);
  });
});

module.exports = app;
