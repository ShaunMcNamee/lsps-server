const {Router} = require('express');
const api = require('./api');

module.exports = models => {
  const router = Router();

  router.use('/api', api(models));

  return router;
};
