const {Router} = require('express');

module.exports = models => {
  let scores = Router();

  scores.get('/', (req, res) => {
    models.Score.findAll().then(scores => res.json(scores));
  });

  return scores;
};
