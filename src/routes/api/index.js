const {Router} = require('express');
const players = require('./players');
const games = require('./games');
const scores = require('./scores');
const standings = require('./standings');

module.exports = models => {
  const api = Router();

  api.use('/players', players(models));

  api.use('/games', games(models));

  api.use('/scores', scores(models));

  api.use('/standings', standings(models));

  return api;
};
