const {Router} = require('express');

module.exports = models => {
  let scores = Router();

  scores.get('/', (req, res) => {
    models.Score.findAll({
      include: [
        {
          model: models.Player,
        },
        {
          model: models.Game,
          attributes: {exclude: ['id', 'season', 'date']},
        },
      ],
      attributes: {exclude: ['id', 'playerId', 'gameId']},
    })
      .then(scores => scores.map(score => score.toJSON()))
      .then(scores => scores.reduce(transformDataIntoPlayerObjectsArray, []).map(calculateGameAndSeasonDataForPlayer))
      .then(scores => res.json(scores));
  });

  return scores;
};

const transformDataIntoPlayerObjectsArray = (acc, score) => {
  const {player: {id: playerId, firstName, lastName}, score: gameScore, game: {number: gameNumber}} = score;
  const scoreObject = {gameScore, gameNumber};
  const currentPlayerExists = acc.some(player => player.playerId === playerId);

  if (currentPlayerExists) {
    return acc.map(player => {
      if (player.playerId === playerId) {
        return {...player, scores: [...player.scores, scoreObject]};
      }

      return player;
    });
  }

  return [...acc, {playerId, playerName: `${firstName} ${lastName}`, scores: [scoreObject]}];
};

const calculateGameAndSeasonDataForPlayer = player => {
  const numberOfScoresCounted = 10;
  const sortedScores = player.scores.slice().sort((score1, score2) => score2.gameScore - score1.gameScore);
  const sortedScoresWithIsUsedSet = sortedScores.map((score, index) => ({
    ...score,
    isUsed: index < numberOfScoresCounted,
  }));
  const lowestCounted =
    sortedScoresWithIsUsedSet.length < numberOfScoresCounted
      ? sortedScoresWithIsUsedSet[sortedScoresWithIsUsedSet.length - 1].gameScore
      : sortedScoresWithIsUsedSet[numberOfScoresCounted - 1].gameScore;
  const sortedScoresWithIsCurrentLowSet = sortedScoresWithIsUsedSet.map(score => ({
    ...score,
    isCurrentLow: score.gameScore === lowestCounted,
  }));
  const totals = sortedScoresWithIsCurrentLowSet.reduce(
    ({totalScore, totalCountedScore}, score) => ({
      totalScore: totalScore + score.gameScore,
      totalCountedScore: totalCountedScore + (score.isUsed ? score.gameScore : 0),
    }),
    {totalScore: 0, totalCountedScore: 0}
  );
  const gamesPlayed = sortedScoresWithIsCurrentLowSet.length;

  return {
    ...player,
    scores: sortedScoresWithIsCurrentLowSet,
    lowestCounted,
    totalScore: totals.totalCountedScore,
    gamesPlayed,
    averageScore: (totals.totalScore / gamesPlayed).toPrecision(4),
  };
};
