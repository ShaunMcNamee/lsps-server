const {Router} = require('express');

module.exports = models => {
  let games = Router();

  games.get('/', (req, res) => {
    models.Game.findAll({
      include: [
        {
          model: models.Score,
        },
      ],
    }).then(games => res.json(games));
  });

  games.post('/', async (req, res) => {
    const date = req.body.gameDate;
    const number = req.body.gameNumber;
    const dateRegex = /\d\d\/\d\d\/\d\d\d\d/;
    if (dateRegex.test(date)) {
      const matchingGames = await models.Game.findAll({where: {date, number}});

      if (matchingGames !== null) {
        models.Game.create({
          date,
          number,
          season: date.split('/')[2],
        });
        models.Game.findAll({
          attributes: ['id, date'],
        }).then(games => res.json(games));
      } else {
        res.status(400).send('Game already exists');
      }
    } else {
      res.status(400).send('Bad Request');
    }
  });

  games.get('/:season', (req, res) => {
    models.Game.findAll({
      where: {
        season: req.params.season,
      },
    }).then(games => res.json(games));
  });

  return games;
};
