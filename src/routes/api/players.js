const {Router} = require('express');

module.exports = models => {
  let players = Router();

  players.get('/', (req, res) => {
    models.Player.findAll({
      include: [
        {
          model: models.Score,
        },
      ],
    }).then(players => res.json(players));
  });

  return players;
};
