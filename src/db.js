const Sequelize = require('sequelize');
const config = require('../config');

const {host, port, name, username, password} = config.database;

module.exports = callback => {
  const sequelize = new Sequelize(name, username, password, {
    host,
    port,
    dialect: 'mysql',
    operatorsAliases: false,

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  });
  sequelize
    .authenticate()
    .then(() => {
      console.log('Connection has been established successfully.');
      callback(sequelize);
    })
    .catch(err => {
      console.error('Unable to connect to the database:', err);
    });
};
