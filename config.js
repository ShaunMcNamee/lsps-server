const fs = require('fs');
const yaml = require('js-yaml');
const path = require('path');

const BASE_PATH = process.cwd();
const configEnv = process.env.CONFIG_ENV || 'development';
const filePath = path.join(BASE_PATH, `config/${configEnv}.yaml`);
const config = yaml.safeLoad(fs.readFileSync(filePath, 'utf8'));
process.env.NODE_ENV = config.nodeEnv || 'development';

module.exports = config;
