LSPS Server

This server retrieves the LSPS data from the database and serves it to the client.

It is currently being built into a docker image when pushed.

License
-------

MIT
